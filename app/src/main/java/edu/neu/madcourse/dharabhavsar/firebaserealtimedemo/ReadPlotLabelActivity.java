package edu.neu.madcourse.dharabhavsar.firebaserealtimedemo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.BufferOverflowException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.zip.GZIPInputStream;

import edu.neu.madcourse.dharabhavsar.firebaserealtimedemo.receiver.NetworkStateChangeReceiver;
import edu.neu.madcourse.dharabhavsar.firebaserealtimedemo.utils.ConnectToFirebaseStorage;
import lecho.lib.hellocharts.gesture.ZoomType;
import lecho.lib.hellocharts.listener.ViewportChangeListener;
import lecho.lib.hellocharts.model.Axis;
import lecho.lib.hellocharts.model.Line;
import lecho.lib.hellocharts.model.LineChartData;
import lecho.lib.hellocharts.model.PointValue;
import lecho.lib.hellocharts.model.Viewport;
import lecho.lib.hellocharts.util.ChartUtils;
import lecho.lib.hellocharts.view.LineChartView;
import lecho.lib.hellocharts.view.PreviewLineChartView;

import static edu.neu.madcourse.dharabhavsar.firebaserealtimedemo.receiver.NetworkStateChangeReceiver.IS_NETWORK_AVAILABLE;

public class ReadPlotLabelActivity extends AppCompatActivity {

    private static final String TAG = ReadPlotLabelActivity.class.getSimpleName();

    private static final int SAMPLING_RATE = 300000; // based on 80Hz sampling rate data generated

    private StorageReference storageRef;

    int counter;

    private Double[] doubleX = new Double[SAMPLING_RATE];
    private Double[] doubleY = new Double[SAMPLING_RATE];
    private Double[] doubleZ = new Double[SAMPLING_RATE];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_read_plot_label);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().add(R.id.container, new PlaceholderFragment()).commit();
        }

        IntentFilter intentFilter = new IntentFilter(NetworkStateChangeReceiver.NETWORK_AVAILABLE_ACTION);
        LocalBroadcastManager.getInstance(this).registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                boolean isNetworkAvailable = intent.getBooleanExtra(IS_NETWORK_AVAILABLE, false);
                String networkStatus = isNetworkAvailable ? "connected" : "disconnected";

                if (networkStatus.equals("connected")) {
                    Snackbar.make(findViewById(R.id.container), "Network Status: " + networkStatus, Snackbar.LENGTH_LONG).show();
                } else {
                    Snackbar.make(findViewById(R.id.container), "Network Status: " + networkStatus, Snackbar.LENGTH_INDEFINITE).show();
                }
            }
        }, intentFilter);

        Intent intent = getIntent();
        String fileName = intent.getStringExtra("FileName");

        FirebaseStorage storage = ConnectToFirebaseStorage.instance(getApplicationContext());

        // Create a storage reference from our app
        storageRef = storage.getReference();

        Arrays.fill(doubleX, 0.0d);
        Arrays.fill(doubleY, 0.0d);
        Arrays.fill(doubleZ, 0.0d);

        // STEP-2 ::: Method to download the file from the Firebase Storage
        downloadFile(fileName);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // If there's a download in progress, save the reference so you can query it later
        if (storageRef != null) {
            outState.putString("reference", storageRef.toString());
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        // If there was a download in progress, get its reference and create a new StorageReference
        final String stringRef = savedInstanceState.getString("reference");
        if (stringRef == null) {
            return;
        }
        storageRef = ConnectToFirebaseStorage.instance(getApplicationContext()).getReferenceFromUrl(stringRef);

        // Find all DownloadTasks under this StorageReference (in this example, there should be one)
        List<FileDownloadTask> tasks = storageRef.getActiveDownloadTasks();
        if (tasks.size() > 0) {
            // Get the task monitoring the download
            FileDownloadTask task = tasks.get(0);

            // Add new listeners to the task using an Activity scope
            task.addOnSuccessListener(this, new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(FileDownloadTask.TaskSnapshot state) {
                    // handleSuccess(state); //call a user defined function to handle the event.
                    Log.d(TAG, "onSuccess: ");
                }
            });
        }
    }

    private void downloadFile(String fileName) {
        Log.d(TAG, "downloadFile: " + fileName);
        StorageReference pathReference = storageRef.child(fileName);

        File localFile;
        try {
            localFile = File.createTempFile(pathReference.getName(), null);
            final File finalLocalFile = localFile;
            pathReference.getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                    // Data for "gz file" is returned, use this as needed
                    Log.d(TAG, "onSuccess: File obtained..... SPACE of internal memory: " + finalLocalFile.getTotalSpace());
                    Log.d(TAG, "onSuccess: File obtained..... NAME: " + finalLocalFile.getName());
                    Log.d(TAG, "onSuccess: PATH: " + finalLocalFile.getPath());

                    // STEP-3 ::: Method to unzip the downloaded file
                    unzipFile(finalLocalFile.getParent(), finalLocalFile.getName());
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    // Handle any errors
                    Log.e(TAG, "onFailure: ", exception);
                }
            }).addOnProgressListener(new OnProgressListener<FileDownloadTask.TaskSnapshot>() {
                @Override
                public void onProgress(FileDownloadTask.TaskSnapshot taskSnapshot) {
//                    Log.d(TAG, "onProgress: " + taskSnapshot.toString());
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void unzipFile(String path, String zipName) {
        String INPUT_GZIP_FILE = path + "/" + zipName;

        InputStream fis;
        GZIPInputStream gis;
        try {
            fis = new FileInputStream(INPUT_GZIP_FILE);
            gis = new GZIPInputStream(new BufferedInputStream(fis));

            Log.d(TAG, "gunzipFile: Gunzipping file");

            InputStreamReader reader = new InputStreamReader(gis);
            BufferedReader bufferReader = new BufferedReader(reader);

            // STEP-4a ::: Method to read data directly from compressed CSV file
//            parser(bufferReader);

        } catch (IOException | BufferOverflowException e) {
            Log.e(TAG, "gunzipFile: ", e);
        }
    }

    private void parser(BufferedReader bufferReader) {
        String line;
        try {
            String[] parts;
            while ((line = bufferReader.readLine()) != null) {
                parts = line.split(",");
//                if (counter < 10) {
//                    Log.d(TAG, "parser: ... " + parts[0] + " .... " + parts[1] + " ... " + parts[2] + " ... " + parts[3]);
//                }
                if (counter > 0) {
                    doubleX[counter] = Double.parseDouble(parts[1]);
                    doubleY[counter] = Double.parseDouble(parts[2]);
                    doubleZ[counter] = Double.parseDouble(parts[3]);
                }
                counter++;
            }

            Log.e(TAG, "parser: counter  = " + counter);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


//    /**
//     * A placeholder fragment containing a simple view.
//     */
//    public static class PlaceholderFragment extends Fragment {
//
//        private ColumnChartView chart;
//        private PreviewColumnChartView previewChart;
//        private ColumnChartData data;
//        /**
//         * Deep copy of data.
//         */
//        private ColumnChartData previewData;
//
//        public PlaceholderFragment() {
//        }
//
//        @Override
//        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//            setHasOptionsMenu(true);
//            View rootView = inflater.inflate(R.layout.fragment_read_plot_label, container, false);
//
//            chart = (ColumnChartView) rootView.findViewById(R.id.chart);
//            previewChart = (PreviewColumnChartView) rootView.findViewById(R.id.chart_preview);
//
//            // Generate data for previewed chart and copy of that data for preview chart.
//            generateDefaultData();
//
//            chart.setColumnChartData(data);
//            // Disable zoom/scroll for previewed chart, visible chart ranges depends on preview chart viewport so
//            // zoom/scroll is unnecessary.
//            chart.setZoomEnabled(false);
//            chart.setScrollEnabled(false);
//
//            previewChart.setColumnChartData(previewData);
//            previewChart.setViewportChangeListener(new ViewportListener());
//
//            previewX(false);
//
//            return rootView;
//        }
//
//        // MENU
//        @Override
//        public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//            inflater.inflate(R.menu.preview_column_chart, menu);
//        }
//
//        @Override
//        public boolean onOptionsItemSelected(MenuItem item) {
//            int id = item.getItemId();
//            if (id == R.id.action_reset) {
//                generateDefaultData();
//                chart.setColumnChartData(data);
//                previewChart.setColumnChartData(previewData);
//                previewX(true);
//                return true;
//            }
//            if (id == R.id.action_preview_both) {
//                previewXY();
//                previewChart.setZoomType(ZoomType.HORIZONTAL_AND_VERTICAL);
//                return true;
//            }
//            if (id == R.id.action_preview_horizontal) {
//                previewX(true);
//                return true;
//            }
//            if (id == R.id.action_preview_vertical) {
//                previewY();
//                return true;
//            }
//            if (id == R.id.action_change_color) {
//                int color = ChartUtils.pickColor();
//                while (color == previewChart.getPreviewColor()) {
//                    color = ChartUtils.pickColor();
//                }
//                previewChart.setPreviewColor(color);
//                return true;
//            }
//            return super.onOptionsItemSelected(item);
//        }
//
//        private void generateDefaultData() {
//            int numSubcolumns = 1;
//            int numColumns = 50;
//            List<Column> columns = new ArrayList<Column>();
//            List<SubcolumnValue> values;
//            for (int i = 0; i < numColumns; ++i) {
//
//                values = new ArrayList<SubcolumnValue>();
//                for (int j = 0; j < numSubcolumns; ++j) {
//                    values.add(new SubcolumnValue((float) Math.random() * 50f + 5, ChartUtils.pickColor()));
//                }
//
//                columns.add(new Column(values));
//            }
//
//            data = new ColumnChartData(columns);
//            data.setAxisXBottom(new Axis());
//            data.setAxisYLeft(new Axis().setHasLines(true));
//
//            // prepare preview data, is better to use separate deep copy for preview chart.
//            // set color to grey to make preview area more visible.
//            previewData = new ColumnChartData(data);
//            for (Column column : previewData.getColumns()) {
//                for (SubcolumnValue value : column.getValues()) {
//                    value.setColor(ChartUtils.DEFAULT_DARKEN_COLOR);
//                }
//            }
//
//        }
//
//        private void previewY() {
//            Viewport tempViewport = new Viewport(chart.getMaximumViewport());
//            float dy = tempViewport.height() / 4;
//            tempViewport.inset(0, dy);
//            previewChart.setCurrentViewportWithAnimation(tempViewport);
//            previewChart.setZoomType(ZoomType.VERTICAL);
//        }
//
//        private void previewX(boolean animate) {
//            Viewport tempViewport = new Viewport(chart.getMaximumViewport());
//            float dx = tempViewport.width() / 4;
//            tempViewport.inset(dx, 0);
//            if (animate) {
//                previewChart.setCurrentViewportWithAnimation(tempViewport);
//            } else {
//                previewChart.setCurrentViewport(tempViewport);
//            }
//            previewChart.setZoomType(ZoomType.HORIZONTAL);
//        }
//
//        private void previewXY() {
//            // Better to not modify viewport of any chart directly so create a copy.
//            Viewport tempViewport = new Viewport(chart.getMaximumViewport());
//            // Make temp viewport smaller.
//            float dx = tempViewport.width() / 4;
//            float dy = tempViewport.height() / 4;
//            tempViewport.inset(dx, dy);
//            previewChart.setCurrentViewportWithAnimation(tempViewport);
//        }
//
//        /**
//         * Viewport listener for preview chart(lower one). in {@link #onViewportChanged(Viewport)} method change
//         * viewport of upper chart.
//         */
//        private class ViewportListener implements ViewportChangeListener {
//
//            @Override
//            public void onViewportChanged(Viewport newViewport) {
//                // don't use animation, it is unnecessary when using preview chart because usually viewport changes
//                // happens to often.
//                chart.setCurrentViewport(newViewport);
//            }
//
//        }
//    }

    /**
     * A fragment containing a line chart and preview line chart.
     */
    public static class PlaceholderFragment extends Fragment {

        private LineChartView chart;
        private PreviewLineChartView previewChart;
        private LineChartData data;
        /**
         * Deep copy of data.
         */
        private LineChartData previewData;

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            setHasOptionsMenu(true);
            View rootView = inflater.inflate(R.layout.fragment_read_plot_label, container, false);

            chart = (LineChartView) rootView.findViewById(R.id.chart);
            previewChart = (PreviewLineChartView) rootView.findViewById(R.id.chart_preview);

            // Generate data for previewed chart and copy of that data for preview chart.
            generateDefaultData();

            chart.setLineChartData(data);
            // Disable zoom/scroll for previewed chart, visible chart ranges depends on preview chart viewport so
            // zoom/scroll is unnecessary.
            chart.setZoomEnabled(false);
            chart.setScrollEnabled(false);

            previewChart.setLineChartData(previewData);
            previewChart.setViewportChangeListener(new ViewportListener());

            previewX(false);

            return rootView;
        }

        // MENU
        @Override
        public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
            inflater.inflate(R.menu.preview_line_chart, menu);
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            int id = item.getItemId();
            if (id == R.id.action_reset) {
                generateDefaultData();
                chart.setLineChartData(data);
                previewChart.setLineChartData(previewData);
                previewX(true);
                return true;
            }
            if (id == R.id.action_preview_both) {
                previewXY();
                previewChart.setZoomType(ZoomType.HORIZONTAL_AND_VERTICAL);
                return true;
            }
            if (id == R.id.action_preview_horizontal) {
                previewX(true);
                return true;
            }
            if (id == R.id.action_preview_vertical) {
                previewY();
                return true;
            }
            if (id == R.id.action_change_color) {
                int color = ChartUtils.pickColor();
                while (color == previewChart.getPreviewColor()) {
                    color = ChartUtils.pickColor();
                }
                previewChart.setPreviewColor(color);
                return true;
            }
            return super.onOptionsItemSelected(item);
        }

        private void generateDefaultData() {
            int numValues = 50;

            List<PointValue> values = new ArrayList<PointValue>();
            for (int i = 0; i < numValues; ++i) {
                values.add(new PointValue(i, (float) Math.random() * 100f));
            }

            Line line = new Line(values);
            line.setColor(ChartUtils.COLOR_GREEN);
            line.setHasPoints(false);// too many values so don't draw points.

            List<Line> lines = new ArrayList<Line>();
            lines.add(line);

            data = new LineChartData(lines);
            data.setAxisXBottom(new Axis());
            data.setAxisYLeft(new Axis().setHasLines(true));

            // prepare preview data, is better to use separate deep copy for preview chart.
            // Set color to grey to make preview area more visible.
            previewData = new LineChartData(data);
            previewData.getLines().get(0).setColor(ChartUtils.DEFAULT_DARKEN_COLOR);

        }

        private void previewY() {
            Viewport tempViewport = new Viewport(chart.getMaximumViewport());
            float dy = tempViewport.height() / 4;
            tempViewport.inset(0, dy);
            previewChart.setCurrentViewportWithAnimation(tempViewport);
            previewChart.setZoomType(ZoomType.VERTICAL);
        }

        private void previewX(boolean animate) {
            Viewport tempViewport = new Viewport(chart.getMaximumViewport());
            float dx = tempViewport.width() / 4;
            tempViewport.inset(dx, 0);
            if (animate) {
                previewChart.setCurrentViewportWithAnimation(tempViewport);
            } else {
                previewChart.setCurrentViewport(tempViewport);
            }
            previewChart.setZoomType(ZoomType.HORIZONTAL);
        }

        private void previewXY() {
            // Better to not modify viewport of any chart directly so create a copy.
            Viewport tempViewport = new Viewport(chart.getMaximumViewport());
            // Make temp viewport smaller.
            float dx = tempViewport.width() / 4;
            float dy = tempViewport.height() / 4;
            tempViewport.inset(dx, dy);
            previewChart.setCurrentViewportWithAnimation(tempViewport);
        }

        /**
         * Viewport listener for preview chart(lower one). in {@link #onViewportChanged(Viewport)} method change
         * viewport of upper chart.
         */
        private class ViewportListener implements ViewportChangeListener {

            @Override
            public void onViewportChanged(Viewport newViewport) {
                // don't use animation, it is unnecessary when using preview chart.
                chart.setCurrentViewport(newViewport);
            }

        }

    }
}
