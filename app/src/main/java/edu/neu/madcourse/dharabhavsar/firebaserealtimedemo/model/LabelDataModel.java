package edu.neu.madcourse.dharabhavsar.firebaserealtimedemo.model;

/**
 * Created by Dhara on 4/27/2017.
 */

public class LabelDataModel {

    private String startTime;
    private String endTime;
    private String labelName;
    private String playerID;
    private String createdTime;
    private boolean type;
    private String updatedTime;

    public LabelDataModel() {
    }

    public LabelDataModel(String startTime, String endTime, String labelName, String playerID, String createdTime, boolean type, String updatedTime) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.labelName = labelName;
        this.playerID = playerID;
        this.createdTime = createdTime;
        this.type = type;
        this.updatedTime = updatedTime;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getLabelName() {
        return labelName;
    }

    public void setLabelName(String labelName) {
        this.labelName = labelName;
    }

    public String getPlayerID() {
        return playerID;
    }

    public void setPlayerID(String playerID) {
        this.playerID = playerID;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public boolean isType() {
        return type;
    }

    public void setType(boolean type) {
        this.type = type;
    }

    public String getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(String updatedTime) {
        this.updatedTime = updatedTime;
    }
}
