package edu.neu.madcourse.dharabhavsar.firebaserealtimedemo.labelMethods;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.joda.time.LocalTime;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

import edu.neu.madcourse.dharabhavsar.firebaserealtimedemo.model.LabelDataModel;
import edu.neu.madcourse.dharabhavsar.firebaserealtimedemo.utils.ConnectToFirebaseDatabase;

import static edu.neu.madcourse.dharabhavsar.firebaserealtimedemo.utils.CommonUtils.getRandomActivity;
import static edu.neu.madcourse.dharabhavsar.firebaserealtimedemo.utils.CommonUtils.getRandomPlayer;
import static edu.neu.madcourse.dharabhavsar.firebaserealtimedemo.utils.CommonUtils.getRandomTime;

/**
 * Created by Dhara on 4/27/2017.
 */

public class DataBaseService extends Service {

    private static final String TAG = DataBaseService.class.getSimpleName();

    // Binder given to clients
    private final IBinder mBinder = new DataBaseBinder();
    // Random number generator
    private final Random mGenerator = new Random();

    /**
     * Class used for the client Binder.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with IPC.
     */
    public class DataBaseBinder extends Binder {
        public DataBaseService getService() {
            // Return this instance of LocalService so clients can call public methods
            return DataBaseService.this;
        }
    }

    private FirebaseDatabase database;
    private DatabaseReference mDatabaseRef;
    private Map<String, LabelDataModel> modelMap = new HashMap<>();

    public void generateDummyLabels(String userID, String year, String month, String day) {
        database = ConnectToFirebaseDatabase.instance(this);
        mDatabaseRef = database.getReference();
        Log.d(TAG, "generateDummyLabels: ");
        Map<String, Object> childUpdates = new HashMap<>();

        LabelDataModel tmp;
        String[] strings;
        String key;

        for (int i = 0; i <= 100; i++) {
            key = mDatabaseRef.child("labels").child(userID).child(year + "-" + month + "-" + day).push().getKey();
            Log.d(TAG, "generateDummyLabels: in loop: " + i);
            strings = getRandomTime();
            tmp = new LabelDataModel(strings[0], strings[1], getRandomActivity(), getRandomPlayer(),
                    new LocalTime().toString(), true, null);
            childUpdates.put(key, tmp);
            mDatabaseRef.child("labels").child(userID).child(year + "-" + month + "-" + day).updateChildren(childUpdates);
        }
//        Log.d(TAG, "generateDummyLabels: modelMap = " + modelMap.size());

//        database.getReference("labels").child(userID).child(year+"-"+month+"-"+day).updateChildren(modelMap);
    }

    public void fetchAllLabelsPerDay(final String userID, final String year, final String month, final String day) {
        database = ConnectToFirebaseDatabase.instance(this);
        mDatabaseRef = database.getReference("labels/" + userID + "/" + year + "-" + month + "-" + day);

        Log.d(TAG, "fetchAllLabelsPerDay: ");

        mDatabaseRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                modelMap.put(dataSnapshot.getKey(), (LabelDataModel) dataSnapshot.getValue());
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        mDatabaseRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.d(TAG, "onDataChange: count = " + dataSnapshot.getChildrenCount());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    /**
     * method for clients
     */
    public int getRandomNumber() {
        return mGenerator.nextInt(100);
    }

    /**
     * Method to query for data labels between start and end times given
     * @param userID - participant id
     * @param year - year of the recorded data
     * @param month - month of the recorded data
     * @param day - day of the recorded data
     * @param startTime - query parameter
     * @param endTime - query parameter
     * */
    public void fetchLabel1(String userID, String year, String month, String day, String startTime, String endTime) {
        Log.d(TAG, "fetchLabel2: result for startTime: " + startTime + " endTime: " + endTime);
        final AtomicInteger count = new AtomicInteger();
        database = ConnectToFirebaseDatabase.instance(this);
        mDatabaseRef = database.getReference("labels/" + userID + "/" + year + "-" + month + "-" + day);

        // includes all the labels with end time within the given start and end time.
        mDatabaseRef.orderByChild("startTime").startAt(startTime)
                .endAt(endTime).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                int intCount = count.getAndIncrement();
                Log.d(TAG, "1... " + dataSnapshot.getKey() + " .... " + intCount);
                // TODO : add to a list
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        // includes all the labels with end time within the given start and end time.
        mDatabaseRef.orderByChild("endTime").startAt(startTime)
                .endAt(endTime).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                int intCount = count.getAndIncrement();
                Log.d(TAG, "1a... " + dataSnapshot.getKey() + " .... " + intCount);
                // TODO : add to the above list - combine the query results
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    /**
    * Method to query for data labels based on LabelName
    * @param userID - participant id
    * @param year - year of the recorded data
    * @param month - month of the recorded data
    * @param day - day of the recorded data
    * @param label - query parameter
    * */
    public void fetchLabel2(String userID, String year, String month, String day, String label) {
        Log.d(TAG, "fetchLabel2: result for label: " + label);
        database = ConnectToFirebaseDatabase.instance(this);
        mDatabaseRef = database.getReference("labels/" + userID + "/" + year + "-" + month + "-" + day);
        mDatabaseRef.orderByChild("labelName").equalTo(label).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Log.d(TAG, dataSnapshot.getKey());
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    /**
     * Method to add a new data label to the existing participant and date
     * @param userID - participant id
     * @param year - year of the recorded data
     * @param month - month of the recorded data
     * @param day - day of the recorded data
     * @param model - adding a new labelDataModel Object in our database
     * */
    public void addLabel(String userID, String year, String month, String day, LabelDataModel model) {
        database = ConnectToFirebaseDatabase.instance(this);
        mDatabaseRef = database.getReference();

        String key = mDatabaseRef.child("labels").child(userID).child(year + "-" + month + "-" + day).push().getKey();
        Log.d(TAG, "addLabel: new key: " + key);
        Map<String, Object> childUpdates = new HashMap<>();

        childUpdates.put(key, model);
        mDatabaseRef.child("labels").child(userID).child(year + "-" + month + "-" + day).updateChildren(childUpdates, new DatabaseReference.CompletionListener() {

            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if(databaseError == null) {
                    Log.d(TAG, "onComplete: new data label added successfully.");
                } else {
                    Log.e(TAG, "onComplete: error: ", databaseError.toException());
                }
            }
        });
    }

    /**
     * Method to delete a newly generated data labels
     * @param userID - participant id
     * @param year - year of the recorded data
     * @param month - month of the recorded data
     * @param day - day of the recorded data
     * */
    public void deleteSelectedLabel(String userID, String year, String month, String day) {
        database = ConnectToFirebaseDatabase.instance(this);
        mDatabaseRef = database.getReference("labels/" + userID + "/" + year + "-" + month + "-" + day);

//        fetchAllLabelsPerDay(userID, year, month, day);
        Log.d(TAG, "deleteSelectedLabel: ");

        String key = ""; // TODO - fetch or pass the key
        mDatabaseRef.child(key).removeValue();
    }

    public void fetchLabel1a(String userID, String year, String month, String day, String startTime, String endTime) {
        Log.d(TAG, "fetchLabel2: result for startTime: " + startTime + " endTime: " + endTime);

//        upvotesRef.runTransaction(new Transaction.Handler() {
//            @Override
//            public Transaction.Result doTransaction(MutableData currentData) {
//                if(currentData.getValue() == null) {
//                    currentData.setValue(1);
//                } else {
//                    currentData.setValue((Long) currentData.getValue() + 1);
//                }
//                return Transaction.success(currentData); //we can also abort by calling Transaction.abort()
//            }
//            @Override
//            public void onComplete(FirebaseError firebaseError, boolean committed, DataSnapshot currentData) {
//                //This method will be called once with the results of the transaction.
//            }
//        });
        fetchAllLabelsPerDay(userID, year, month, day);
    }
}
