package edu.neu.madcourse.dharabhavsar.firebaserealtimedemo;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import org.joda.time.LocalTime;

import edu.neu.madcourse.dharabhavsar.firebaserealtimedemo.labelMethods.DataBaseService;
import edu.neu.madcourse.dharabhavsar.firebaserealtimedemo.model.LabelDataModel;

import static edu.neu.madcourse.dharabhavsar.firebaserealtimedemo.utils.CommonUtils.getRandomActivity;
import static edu.neu.madcourse.dharabhavsar.firebaserealtimedemo.utils.CommonUtils.getRandomPlayer;
import static edu.neu.madcourse.dharabhavsar.firebaserealtimedemo.utils.CommonUtils.getRandomTime;

public class LabelsActivity extends AppCompatActivity {

    DataBaseService mService;
    boolean mBound = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_labels);
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Bind to LocalService
        Intent intent = new Intent(this, DataBaseService.class);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        // Unbind from the service
        if (mBound) {
            unbindService(mConnection);
            mBound = false;
        }
    }

    /** Called when a button is clicked (the button in the layout file attaches to
     * this method with the android:onClick attribute) */
    public void onButtonClick(View v) {
        if (mBound) {
            // Call a method from the LocalService.
            // However, if this call were something that might hang, then this request should
            // occur in a separate thread to avoid slowing down the activity performance.
            mService.generateDummyLabels("participant1234", "2016", "15", "09");
            int num = mService.getRandomNumber();
            Toast.makeText(this, "number: " + num, Toast.LENGTH_SHORT).show();
        }
    }

    /** Defines callbacks for service binding, passed to bindService() */
    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            DataBaseService.DataBaseBinder binder = (DataBaseService.DataBaseBinder) service;
            mService = binder.getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };

    public void fetchData1(View view) {
        mService.fetchLabel1("participant1234", "2016", "15", "09", "04:28:22", "06:55:38");
    }

    public void fetchData2(View view) {
        mService.fetchLabel2("participant1234", "2016", "15", "09", "sedentary");
    }

    public void addLabel(View view) {
        String[] strings = getRandomTime();
        LabelDataModel tmp = new LabelDataModel(strings[0], strings[1], getRandomActivity(), getRandomPlayer(),
                new LocalTime().toString(), true, null);
        mService.addLabel("participant1234", "2016", "15", "09", tmp);
    }

    public void deleteLabel(View view) {
//        mService.generateDummyLabels("participant1234", "2016", "15", "09");
    }
}
