package edu.neu.madcourse.dharabhavsar.firebaserealtimedemo.utils;

import org.joda.time.Hours;
import org.joda.time.LocalTime;
import org.joda.time.Minutes;
import org.joda.time.Seconds;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.Random;

/**
 * Created by Dhara on 4/27/2017.
 */

public class CommonUtils {

    public static String[] getRandomTime() {
        String[] strArr = new String[2];
        final Random random = new Random();
        LocalTime startTime = new LocalTime(random.nextLong()).withMillisOfSecond(0);

        Minutes minimumPeriod = Minutes.TWO;
        int minimumPeriodInSeconds = minimumPeriod.toStandardSeconds().getSeconds();
        int maximumPeriodInSeconds = Hours.ONE.toStandardSeconds().getSeconds();

        Seconds randomPeriod = Seconds.seconds(random.nextInt(maximumPeriodInSeconds - minimumPeriodInSeconds));
        LocalTime endTime = startTime.plus(minimumPeriod).plus(randomPeriod);

        DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("HH:mm:ss");
        strArr[0] = dateTimeFormatter.print(startTime);
        strArr[1] = dateTimeFormatter.print(endTime);

        return strArr;
    }

    public static String getRandomActivity() {
        final String[] wordList = {"wear", "non-wear", "sedentary", "physical activity"};
        return wordList[new Random().nextInt(wordList.length)];
    }

    public static String getRandomPlayer() {
        final String[] wordList = {"player1", "player2", "player3", "player4", "player5", "player6", "player7", "player8"};
        return wordList[new Random().nextInt(wordList.length)];
    }

}
