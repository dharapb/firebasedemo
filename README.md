# AccelerationDemo2017
Demo to connect to Firebase Storage to select a particular .csv.gz file, fetch and plot acceleration data from it.

> ### Updated:
>
> 1. Code added to enable you to select and download a particular file from firebase.
> 2. Removed ***google-services.json*** file to enable you to connect to your firebase simultaneously. _(Also to make this Demo code run separately as well as include the code in your app, you will need to add your google-services.json file to the app folder.)_
> 3. You will need to copy paste the **utils** package to make the connection possible to this hosting Firebase Storage, Database and Auth.

### Methods for accessing Data Labels in Firebase Database
`/labelMethods/DataBaseService.java`

### Methods for accessing Data Labels in Firebase Database
'/labelMethods/DataBaseService.java'

[Updated Details Document](https://docs.google.com/a/husky.neu.edu/document/d/13L9Upy_eWb6_1I3X2nJDx-MKMjQGcv-p0p7iYgIwCoQ/edit?usp=sharing)